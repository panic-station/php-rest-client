<?php

namespace PanicStation\RestClient;



use
    http;
use
    PanicStation\HttpErrorsExceptions\HttpExceptionFactory;
use
    PanicStation\RestClient\DataHandler\IDataHandler;
use
    Psr\Log\LoggerAwareInterface;
use
    Psr\Log\LoggerInterface;
use
    Psr\Log\LogLevel;

/**
 * Implementation of basic REST client.
 *
 * @link http://www.php-fig.org/psr/psr-3/
 *
 * @package PanicStation\RestClient
 */
class RestClient implements IRestClient, LoggerAwareInterface
{

    /**
     * Base URL that any specific method URL will be prepended with
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * Array of permanent data to be sent with any POST or PUT request
     *
     * @var array
     */
    protected $data = Array();

    /**
     * Instance of data handler that will be used to prepare data for POST or
     * PUT request and parse the response
     *
     * @var IDataHandler
     */
    protected $dataHandler;

    /**
     * Array of permanent headers to be sent with each request
     *
     * @var array
     */
    protected $headers = Array();

    /**
     * Instance of http\Client that all request will be processed through
     *
     * @var http\Client
     */
    protected $httpClient;

    /**
     * Instance of PSR-3 compatible logger that will be used to log requests
     *
     * @var Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Array of permanent query data that will be sent with each request
     *
     * @var array
     */
    protected $queryData = Array();


    /**
     * Instantiates and configures the Rest Client
     *
     * @param string $baseUrl Base URL that any specific method URL will be
     * prepended with
     *
     * @param IDataHandler $dataHandler Instance of data handler that will be
     * used to prepare data for POST or PUT request and parse the response
     *
     * @param LoggerInterface $logger Instance of PSR-3 compatible logger that
     * will be used to log requests
     */
    public function __construct(
        $baseUrl,
        IDataHandler $dataHandler,
        LoggerInterface $logger = null
    ) {
        $this->setBaseUrl( $baseUrl );

        $this->setDataHandler( $dataHandler );

        if ( $logger !== null )
        {
            $this->setLogger( $logger );
        }

        $this->httpClient = new http\Client();
    }


    /**
     * Adds data that needs to be send with each POST of PUT request.
     *
     * @param array $data Key => Value pairs of permanent parameters and their
     * values
     *
     * @return IRestClient self
     */
    public function addPermanentData( Array $data )
    {
        $this->data = array_merge(
            $this->data,
            $data
        );

        return $this;
    }


    /**
     * Adds headers that need to be sent with each request.
     *
     * @param array $headers Key => Value pairs of permanent headers to send
     *
     * @return IRestClient self
     */
    public function addPermanentHeaders( Array $headers )
    {
        $this->headers = array_merge(
            $this->headers,
            $headers
        );

        return $this;
    }


    /**
     * Adds query data that needs to be sent with each request
     *
     * @param array $queryData Key => Value pairs of permanent query parameters
     * and their values
     *
     * @return IRestClient self
     */
    public function addPermanentQueryData( Array $queryData )
    {
        $this->queryData = array_merge(
            $this->queryData,
            $queryData
        );

        return $this;
    }


    /**
     * Deletes the entire collection OR Deletes the addressed member of the
     * collection.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param Array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @param array Key => Value pairs of additional headers to send
     *
     * @return mixed Raw or processed by data handler response
     */
    public function delete(
        $url,
        Array $headers = Array(),
        $rawResponse = false
    ) {

        return $this->request(
            'DELETE',
            $url,
            Array(),
            Array(),
            $headers,
            $rawResponse
        );
    }


    /**
     * Lists the URIs and perhaps other details of the collection's
     * members OR retrieves a representation of the addressed member of the
     * collection, expressed in an appropriate Internet media type.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $queryData Key => Value pairs of filtering options
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return mixed Raw or processed by data handler response
     */
    public function get(
        $url,
        Array $queryData = Array(),
        Array $headers = Array(),
        $rawResponse = false
    ) {

        return $this->request(
            'GET',
            $url,
            $queryData,
            Array(),
            $headers,
            $rawResponse
        );
    }


    /**
     * Creates a new entry in the collection. The new entry's URI is assigned
     * automatically and is usually returned by the operation.
     *
     * Not generally used in application to collection members.
     * Treats the addressed member as a collection in its own right and create a
     * new entry in it.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $data Key => Value pairs of new entry attributes and their
     * values
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return mixed Raw or processed by data handler response
     */
    public function post(
        $url,
        Array $data,
        Array $headers = Array(),
        $rawResponse = false
    ) {

        return $this->request(
            'POST',
            $url,
            Array(),
            $data,
            $headers,
            $rawResponse
        );
    }


    /**
     * Replaces the entire collection with another collection. OR replaces the
     * addressed member of the collection, or if it does not exist, creates it.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $data Key => Value pairs of entry attributes and their
     * values
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return mixed Raw or processed by data handler response
     */
    public function put(
        $url,
        Array $data,
        Array $headers = Array(),
        $rawResponse = false
    ) {

        return $this->request(
            'PUT',
            $url,
            Array(),
            $data,
            $headers,
            $rawResponse
        );
    }


    /**
     * Sets the base URL that will be prepended to each request's URL
     *
     * @param string $baseUrl Base URL
     *
     * @return IRestClient self
     */
    public function setBaseUrl( $baseUrl )
    {
        $this->baseUrl =
            rtrim(
                trim( $baseUrl ),
                '/'
            )
            .'/';

        return $this;
    }


    /**
     * Sets the data handler to prepare data to be sent via POST or PUT and
     * parses a response
     *
     * @param IDataHandler $dataHandler Data handler instance
     *
     * @return IRestClient self
     */
    public function setDataHandler(
        IDataHandler $dataHandler
    ) {
        $this->dataHandler = $dataHandler;

        $this->updateContentHeaders();

        return $this;
    }


    /**
     * Sets a logger instance
     *
     * @param \Psr\Log\LoggerInterface $logger
     *
     * @return IRestClient self
     */
    public function setLogger( LoggerInterface $logger )
    {
        $this->logger = $logger;

        return $this;
    }


    /**
     * Sets data that needs to be send with each POST of PUT request.
     *
     * Replaces all previously set or added data.
     *
     * @param array $data Key => Value pairs of permanent parameters and their
     * values
     *
     * @return IRestClient self
     */
    public function setPermanentData( Array $data )
    {
        $this->data = $data;

        return $this;
    }


    /**
     * Sets headers that need to be sent with each request.
     *
     * Replaces all previously set or added headers.
     *
     * @param array $headers Key => Value pairs of permanent headers to send
     *
     * @return IRestClient self
     */
    public function setPermanentHeaders( Array $headers )
    {
        $this->headers = $headers;

        return $this;
    }


    /**
     * Sets query data that needs to be sent with each request
     *
     * Replaces all previously set or added query data.
     *
     * @param array $queryData Key => Value pairs of permanent query parameters
     * and their values
     *
     * @return IRestClient self
     */
    public function setPermanentQueryData( Array $queryData )
    {
        $this->queryData = $queryData;

        return $this;
    }


    /**
     * Prepares a request instance based on given data
     *
     * @param string $requestMethod Method to be used for the request
     *
     * @param string $url Requested URL
     *
     * @param array $queryData Query parameters to send
     *
     * @param array $data Data to send in the request body
     *
     * @param array $headers Headers to send withing the request
     *
     * @return \http\Client\Request An instance of prepared request
     */
    protected function buildRequest(
        $requestMethod,
        $url,
        Array $queryData,
        Array $data,
        Array $headers
    ) {

        $request = new http\Client\Request();

        $request
            ->setRequestMethod( $requestMethod )

            ->setRequestUrl( $this->baseUrl.$url )

            ->addQuery( $this->queryData )
            ->addQuery( $queryData )

            ->addHeaders( $this->headers )
            ->addHeaders( $headers );

        if ( $requestMethod == 'POST' || $requestMethod == 'PUT' )
        {
            if ( !empty( $this->data ) )
            {
                $request->getBody()->append(
                    new http\Message( $this->dataHandler->prepareRequest(
                        $this->data
                    ) )
                );

            }

            if ( !empty( $data ) )
            {
                $request->getBody()->append(
                    new http\Message( $this->dataHandler->prepareRequest(
                        $data
                    ) )
                );
            }
        }

        return $request;
    }


    /**
     * Decorator method for \Psr\Log\LoggerInterface::log().
     *
     * Checks if logger instance was set and if so, passes all the data to it
     *
     * @see \Psr\Log\LoggerInterface::log()
     *
     * @param mixed $level Log level
     *
     * @param string $message Message to log
     *
     * @param array $context Context of the message
     *
     * @return IRestClient self
     */
    protected function log(
        $level,
        $message,
        Array $context = array()
    ) {

        if ( $this->logger )
        {
            $this->logger->log(
                $level,
                $message,
                $context
            );
        }

        return $this;
    }


    /**
     * Sends prepared request and return raw response back.
     *
     * @param \http\Client\Request $request Request to send
     *
     * @return \http\Client\Response Raw response
     *
     * @throws \PanicStation\HttpErrorsExceptions\IHttpErrorException
     */
    protected function processRequest( http\Client\Request $request )
    {
        $logContext = Array(
            'method' => $request->getRequestMethod(),
            'url' => $request->getRequestUrl(),
        );

        $this->log(
            LogLevel::INFO,
            'Performing {method} request to {url}',
            $logContext
        );

        $this->log(
            LogLevel::DEBUG,
            'Raw request: {request}',
            Array(
                'request' => print_r(
                    $request,
                    true
                )
            )
        );

        $this->httpClient->enqueue( $request )->send();

        $response = $this->httpClient->getResponse();

        $this->log(
            LogLevel::DEBUG,
            'Raw response: {response}',
            Array(
                'response' => print_r(
                    $response,
                    true
                )
            )
        );

        $responseCode = (int)$response->getTransferInfo( 'response_code' );

        if ( HttpExceptionFactory::isErrorCode( $responseCode ) )
        {
            $exception = HttpExceptionFactory::createException( $responseCode );

            $this->log(
                LogLevel::ERROR,
                '{method} request to {url} failed with code {code}',
                array_merge(
                    $logContext,
                    Array(
                        'code' => $responseCode,
                        'exception' => $exception
                    )
                )
            );

            throw $exception;
        }

        $this->log(
            LogLevel::INFO,
            '{method} request to {url} finished successfully',
            $logContext
        );
        return $response;
    }


    /**
     * Sends the raw response body to the data handler to parse.
     *
     * @param http\Client\Response $response Response to send to data handler
     *
     * @return mixed Parsed response body
     */
    protected function processResponse( http\Client\Response $response )
    {
        return $this->dataHandler->parseResponse(
            $response->getBody()->toString()
        );
    }


    /**
     * Encapsulates just high level logic of communication with server.
     *
     * @param string $requestMethod Method to be used for the request
     *
     * @param string $url Requested URL
     *
     * @param array $queryData Query parameters to send
     *
     * @param array $data Data to send in the request body
     *
     * @param array $headers Headers to send withing the request
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @return http\Client\Response|mixed
     */
    protected function request(
        $requestMethod,
        $url,
        Array $queryData,
        Array $data,
        Array $headers,
        $rawResponse
    ) {
        $request = $this->buildRequest(
            $requestMethod,
            $url,
            $queryData,
            $data,
            $headers
        );

        $response = $this->processRequest( $request );


        if ( $rawResponse )
        {
            $result = $response;
        }
        else
        {
            $result = $this->processResponse( $response );
        }

        return $result;
    }


    /**
     * Sets permanent headers with appropriate content type according to
     * specified data handler.
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.2
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
     *
     */
    protected function updateContentHeaders()
    {
        $contentType = $this->dataHandler->getContentType();
        $contentCharset = $this->dataHandler->getContentCharset();

        $headers = Array();

        if ( !empty( $contentType ) )
        {
            $headers['Accept'] = $contentType;
            $headers['Content-type'] = $contentType;
        }

        if ( !empty( $contentCharset ) )
        {
            $headers['Accept-Charset'] = $contentCharset;

            if ( isset( $headers['Content-type'] ) )
            {
                $headers['Content-type'] .= '; charset='.$contentCharset;
            }
        }

        if ( !empty( $headers ) )
        {
            $this->addPermanentHeaders( $headers );
        }
    }
} 