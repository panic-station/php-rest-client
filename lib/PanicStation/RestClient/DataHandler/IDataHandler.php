<?php


namespace PanicStation\RestClient\DataHandler;



/**
 * Describes general data handler to prepare data to send with request body and
 * to parse response.
 *
 * Meant to be used for implementation of communication protocols like XML of
 * JSON
 *
 * @package PanicStation\RestClient\DataHandler
 */
interface IDataHandler
{

    
    /**
     * Supposed to return content charset that suitable to be used in
     * Accept-Charset and Content-type HTTP headers
     *
     * @example 'UTF-8'
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.2
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
     *
     * @return string Content charset
     */
    public function getContentCharset();


    /**
     * Supposed to return content type that suitable to be used in
     * Accept and Content-type HTTP headers
     *
     * @example 'application/json'
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
     *
     * @return string Content type
     */
    public function getContentType();


    /**
     * Parses a raw response body and returns results back.
     *
     * Supposed to be used to decode specific protocol (XML, JSON etc.) to
     * native PHP entities (arrays, object etc.).
     *
     * @param string $response Raw response body
     *
     * @return mixed Decoded data
     */
    public function parseResponse( $response );


    /**
     * Encodes data according to specific protocol.
     *
     * Supposed to be used to encode native PHP entities (arrays, object etc.)
     * with specific protocol (XML, JSON etc.)
     *
     * @param array $data Data to encode
     *
     * @return string Encoded data
     */
    public function prepareRequest( Array $data );

} 