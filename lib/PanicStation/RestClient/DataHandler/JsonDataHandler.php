<?php


namespace PanicStation\RestClient\DataHandler;



/**
 * Implementation of JSON protocol data handler
 *
 * @package PanicStation\RestClient\DataHandler
 */
class JsonDataHandler implements IDataHandler
{

    
    /**
     * Returns content charset that supposed to be used in Accept-Charset and
     * Content-type HTTP headers
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.2
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
     *
     * @return string Content charset
     */
    public function getContentCharset()
    {
        return 'UTF-8';
    }


    /**
     * Returns content type that supposed to be used in Accept and
     * Content-type HTTP headers
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
     *
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
     *
     * @return string Content type
     */
    public function getContentType()
    {
        return 'application/json';
    }


    /**
     * Decodes raw response body from JSON and returns as associative array
     *
     * @param string $response Raw response body
     *
     * @return Array Decoded data
     */
    public function parseResponse( $response )
    {
        $result = '';

        if ( !empty( $response ) )
        {
            $result = json_decode(
                $response,
                true
            );
        }

        return $result;
    }


    /**
     * Encodes data with JSON
     *
     * @param array $data Data to encode
     *
     * @return string Encoded data
     */
    public function prepareRequest( Array $data )
    {
        $result = '';

        if ( !empty( $data ) )
        {
            $result = json_encode( $data );
        }

        return $result;
    }

}