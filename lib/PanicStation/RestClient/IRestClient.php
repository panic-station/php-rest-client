<?php

namespace PanicStation\RestClient;



/**
 * Declares an interface of a simple REST client
 *
 * @package PanicStation\RestClient
 *
 * @link http://en.wikipedia.org/wiki/Representational_state_transfer
 */
interface IRestClient
{

    /**
     * Adds data that needs to be send with each POST of PUT request.
     *
     * @param array $data Key => Value pairs of permanent parameters and their
     * values
     *
     * @return IRestClient self
     */
    public function addPermanentData( Array $data );


    /**
     * Adds headers that need to be sent with each request.
     *
     * @param array $headers Key => Value pairs of permanent headers to send
     *
     * @return IRestClient self
     */
    public function addPermanentHeaders( Array $headers );


    /**
     * Adds query data that needs to be sent with each request
     *
     * @param array $queryData Key => Value pairs of permanent query parameters
     * and their values
     *
     * @return IRestClient self
     */
    public function addPermanentQueryData( Array $queryData );


    /**
     * Deletes the entire collection OR Deletes the addressed member of the
     * collection.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param Array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @param array Key => Value pairs of additional headers to send
     */
    public function delete(
        $url,
        Array $headers = Array(),
        $rawResponse = false
    );


    /**
     * Lists the URIs and perhaps other details of the collection's
     * members OR retrieves a representation of the addressed member of the
     * collection, expressed in an appropriate Internet media type.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $queryData Key => Value pairs of filtering options
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return mixed
     */
    public function get(
        $url,
        Array $queryData = Array(),
        Array $headers = Array(),
        $rawResponse = false
    );


    /**
     * Creates a new entry in the collection. The new entry's URI is assigned
     * automatically and is usually returned by the operation.
     *
     * Not generally used in application to collection members.
     * Treats the addressed member as a collection in its own right and create a
     * new entry in it.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $data Key => Value pairs of new entry attributes and their
     * values
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return string URI of newly created collection member
     */
    public function post(
        $url,
        Array $data,
        Array $headers = Array(),
        $rawResponse = false
    );


    /**
     * Replaces the entire collection with another collection. OR replaces the
     * addressed member of the collection, or if it does not exist, creates it.
     *
     * @param string $url Collection URI, such as http://example.com/resources
     * OR Element URI, such as http://example.com/resources/item17
     *
     * @param array $data Key => Value pairs of entry attributes and their
     * values
     *
     * @param array $headers Key => Value pairs of additional headers to send
     *
     * @param bool $rawResponse If true - raw response will be returned,
     * otherwise will be returned only response body content
     *
     * @link http://devel-m6w6.rhcloud.com/mdref/http/Client/Response/
     *
     * @return mixed URI of newly created collection member or nothing
     */
    public function put(
        $url,
        Array $data,
        Array $headers = Array(),
        $rawResponse = false
    );


    /**
     * Sets the base URL that will be prepended to each request's URL
     *
     * @param string $baseUrl Base URL
     *
     * @return IRestClient self
     */
    public function setBaseUrl( $baseUrl );


    /**
     * Sets data that needs to be send with each POST of PUT request.
     *
     * Replaces all previously set or added data.
     *
     * @param array $data Key => Value pairs of permanent parameters and their
     * values
     *
     * @return IRestClient self
     */
    public function setPermanentData( Array $data );


    /**
     * Sets headers that need to be sent with each request.
     *
     * Replaces all previously set or added headers.
     *
     * @param array $headers Key => Value pairs of permanent headers to send
     *
     * @return IRestClient self
     */
    public function setPermanentHeaders( Array $headers );


    /**
     * Sets query data that needs to be sent with each request
     *
     * Replaces all previously set or added query data.
     *
     * @param array $queryData Key => Value pairs of permanent query parameters
     * and their values
     *
     * @return IRestClient self
     */
    public function setPermanentQueryData( Array $queryData );
}